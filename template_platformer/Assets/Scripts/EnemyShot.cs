using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShot : MonoBehaviour
{
    public GameObject projectilePrefab;
    public float timeBetweenShots;
    public float projectileSpeed;

    private float timer;

    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= timeBetweenShots)
        {
            timer = 0f;
            FireProjectile();
        }
    }

    void FireProjectile()
    {
        GameObject projectile = Instantiate(projectilePrefab, transform.position, Quaternion.identity);
        Rigidbody projectileRigidbody = projectile.GetComponent<Rigidbody>();
        projectileRigidbody.velocity = transform.forward * projectileSpeed;
    }
}