using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDestroyer : MonoBehaviour
{
    public GameObject enemyToDestroy; // Enemigo a destruir

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) // Si el objeto que pas� por encima tiene el tag "Player"
        {
            Destroy(enemyToDestroy); // Destruye el enemigo especificado
        }
    }
}
