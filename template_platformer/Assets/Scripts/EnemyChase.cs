using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyChase : MonoBehaviour
{
    public Transform respawnPoint; // Punto de respawn
    public float speed = 5f; // Velocidad a la que se mueve el enemigo
    public LayerMask playerLayer; // Capa en la que se encuentra el jugador
    public float detectionRadius = 5f; // Radio de detecci�n del jugador

    private Vector3 startingPosition; // Posici�n inicial del enemigo

    private void Start()
    {
        startingPosition = transform.position; // Guarda la posici�n inicial
    }

    private void Update()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, detectionRadius, playerLayer); // Busca colisionadores dentro del radio de detecci�n y en la capa del jugador
        if (colliders.Length > 0) // Si se encontr� al menos un colisionador
        {
            Vector3 direction = (colliders[0].transform.position - transform.position).normalized; // Calcula la direcci�n hacia el jugador
            transform.position += direction * speed * Time.deltaTime; // Se mueve hacia el jugador
            RespawnPlayer(colliders[0].gameObject); // Llama a la funci�n de respawn
        }
    }

    private void RespawnPlayer(GameObject player)
    {
        player.transform.position = respawnPoint.position; // Cambia la posici�n del jugador al punto de respawn
    }
}

