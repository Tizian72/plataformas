using UnityEngine;

public class Enemigo : MonoBehaviour
{
    public float velocidadMovimiento = 5f;  // Velocidad de movimiento del enemigo.
    public float distanciaRaycast = 1f;     // Distancia del raycast para detectar el suelo y el techo.
    public LayerMask capasSueloYTecho;       // Capas del suelo y el techo para detectar con el raycast.
    public string nombreTagJugador = "Player";  // Nombre del tag del jugador.
    public Transform posicionInicio;  // Posici�n de inicio del jugador.

    private Rigidbody rb;                   // Componente Rigidbody del enemigo.
    private Vector3 direccionMovimiento;    // Direcci�n de movimiento del enemigo.

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        direccionMovimiento = Vector3.down; // Inicialmente, el enemigo se mueve hacia abajo.
    }

    void FixedUpdate()
    {
        // Detectar si hay suelo o techo.
        RaycastHit hit;
        bool haySuelo = Physics.Raycast(transform.position, Vector3.down, out hit, distanciaRaycast, capasSueloYTecho);
        bool hayTecho = Physics.Raycast(transform.position, Vector3.up, out hit, distanciaRaycast, capasSueloYTecho);

        // Cambiar la direcci�n de movimiento si se detecta suelo o techo.
        if (haySuelo)
        {
            direccionMovimiento = Vector3.up;
        }
        else if (hayTecho)
        {
            direccionMovimiento = Vector3.down;
        }

        // Mover el enemigo en la direcci�n de movimiento.
        rb.MovePosition(transform.position + direccionMovimiento * velocidadMovimiento * Time.fixedDeltaTime);
    }

    void OnCollisionEnter(Collision collision)
    {
        // Verificar si el objeto que choca es el jugador.
        if (collision.collider.CompareTag(nombreTagJugador))
        {
            // Mover el jugador a la posici�n de inicio.
            collision.transform.position = posicionInicio.position;
        }
    }
}