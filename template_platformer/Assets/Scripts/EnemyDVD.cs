using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDVD : MonoBehaviour
{
    public float velocidadMovimiento = 5f;  // Velocidad de movimiento del enemigo.
    public float distanciaRaycast = 1f;     // Distancia del raycast para detectar las paredes.
    public LayerMask capasParedes;          // Capas de las paredes para detectar con el raycast.

    private Rigidbody rb;                   // Componente Rigidbody del enemigo.
    private Vector3 direccionMovimiento;    // Direcci�n de movimiento del enemigo.
    private Vector3 posicionInicial;        // Posici�n inicial del enemigo.

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        posicionInicial = transform.position;
        direccionMovimiento = CalcularNuevaDireccionMovimiento();
    }

    void FixedUpdate()
    {
        // Detectar si hay paredes.
        RaycastHit hit;
        bool hayParedDerecha = Physics.Raycast(transform.position, transform.right, out hit, distanciaRaycast, capasParedes);
        bool hayParedIzquierda = Physics.Raycast(transform.position, -transform.right, out hit, distanciaRaycast, capasParedes);
        bool hayParedArriba = Physics.Raycast(transform.position, transform.up, out hit, distanciaRaycast, capasParedes);
        bool hayParedAbajo = Physics.Raycast(transform.position, -transform.up, out hit, distanciaRaycast, capasParedes);

        // Cambiar la direcci�n de movimiento si se detecta una pared.
        if (hayParedDerecha || hayParedIzquierda || hayParedArriba || hayParedAbajo)
        {
            direccionMovimiento = CalcularNuevaDireccionMovimiento();
        }

        // Mover el enemigo en la direcci�n de movimiento.
        rb.MovePosition(transform.position + direccionMovimiento * velocidadMovimiento * Time.fixedDeltaTime);
    }

    // Calcula una nueva direcci�n de movimiento al azar.
    private Vector3 CalcularNuevaDireccionMovimiento()
    {
        float x = Random.Range(-1f, 1f);
        float y = Random.Range(-1f, 1f);
        float z = Random.Range(-1f, 1f);

        return new Vector3(x, y, z).normalized;
    }

    // Cuando el jugador entra en contacto con el collider del enemigo, teleporta al jugador al punto de respawn.
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador"))
        {
            other.gameObject.transform.position = posicionInicial;
        }
    }
}
