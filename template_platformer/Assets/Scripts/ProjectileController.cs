using UnityEngine;

public class ProjectileController : MonoBehaviour
{
    public LayerMask playerLayer;
    public float lifetime = 5f;
    public Transform respawnPoint; // Variable para el punto de respawn

    private float timer;

    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= lifetime)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (playerLayer == (playerLayer | (1 << other.gameObject.layer)))
        {
            // El proyectil ha chocado con un objeto en la capa "Player"
            // Enviamos al jugador al punto de respawn
            other.transform.position = respawnPoint.position;
        }

        // Destruir el proyectil despu�s de chocar
        Destroy(gameObject);
    }
}