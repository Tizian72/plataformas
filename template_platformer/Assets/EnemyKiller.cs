using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyKiller : MonoBehaviour
{
    public float speed = 1f; // Velocidad a la que se mueve el enemigo
    public float leftBound = -5f; // L�mite izquierdo del movimiento
    public float rightBound = 5f; // L�mite derecho del movimiento
    public int maxPlayerHealth = 1; // Vida m�xima del jugador
    public GameObject gameOverScreen; // Pantalla de game over

    private int playerCount; // Contador de jugadores
    private Rigidbody rb; // Rigidbody del enemigo

    private void Start()
    {
        rb = GetComponent<Rigidbody>(); // Obtiene el Rigidbody del enemigo
        playerCount = GameObject.FindGameObjectsWithTag("Player").Length; // Cuenta el n�mero de jugadores
    }

    private void FixedUpdate()
    {
        // Mueve al enemigo de izquierda a derecha
        Vector3 newPosition = transform.position;
        newPosition.x += speed * Time.fixedDeltaTime;
        if (newPosition.x < leftBound || newPosition.x > rightBound)
        {
            speed = -speed;
        }
        rb.MovePosition(newPosition);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player")) // Si choca con un objeto que tenga el tag "Player"
        {
            Destroy(collision.gameObject); // Destruye al jugador
            playerCount--; // Reduce el contador de jugadores
            if (playerCount <= 0) // Si ya no hay m�s jugadores
            {
                gameOverScreen.SetActive(true); // Muestra la pantalla de game over
            }
        }
    }
}